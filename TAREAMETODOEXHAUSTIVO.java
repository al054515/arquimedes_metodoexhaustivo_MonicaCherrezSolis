
package tarea.metodo.exhaustivo;

/**
 *
 * @author monic
 */
public class TAREAMETODOEXHAUSTIVO {

    public static double z;
    
    public static void Op(int lados, double in){
        int Lados= lados;
        
        double D= in,x,a,b,NuevoL,P,PD;
        
        x = ((D)/2); 
        a= Math.sqrt((1)-(Math.pow(x,2)));
        b= (1-a);
        NuevoL= Math.sqrt((Math.pow(x,2))+(Math.pow(b,2)));    //operaciones.
        P= Lados*D;
        PD= ((P)/2);
        
        z= NuevoL; 
        
        
        System.out.println("Lados:"+Lados+" Lado Inicial:"+D+" x:"+x+" a:"+a+" b:"+b+" Nuevo Lado:"+NuevoL+" P:"+P+"\n VALOR DE PI:"+PD);
    }
    public static void main(String[] args) {
        System.out.println("MÉTODO EXHAUSTIVO\n");
        int NL=6; //Numero de Lados.
        double LI=1; //Lado inicial.
        
        for (int i=0;i<30;i++){
            Op(NL,LI);//hacemos las operaciones con el nuevo número de lados y Los nuevos Lados Iniciales.
            NL= NL*2; //nuestro numero de lados lo multiplicamos por 2
            LI= z; // nuestro lado inicial sera z que fue el que sacamos anteriormente.
            System.out.println("____________________________________________________________________________________________________________");
        }
    }
    
}
